const { resolve } = require('path');

module.exports = {
  head: {
    title: `Jay McMullen - Website's Done Right`,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'Superb sites that load instantly, respond quickly, feel natrual and provide an immersive user experience.',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  loading: { color: '#3B8070' },
  css: ['normalize.css', '~/assets/styles/theme.scss'],
  sassResources: [
    resolve(__dirname, 'assets/styles/settings/variables.scss'),
    resolve(__dirname, 'assets/styles/settings/utilities.scss'),
  ],
  modules: ['nuxt-sass-resources-loader'],
  build: {
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
  },
};
